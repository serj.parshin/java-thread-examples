package ru.parshin.example;

import java.time.Instant;

public class SimpleTask implements Runnable {

    private final String name;
    private Integer result = 0;

    public SimpleTask(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        System.out.println("Задача " + name + " начата. В " + Instant.now());
        for (int i = 0; i < 1000; i++) {
            this.result += i;
        }
        System.out.println("Задача " + name + " завершена с результатом: " + result);
    }
}
