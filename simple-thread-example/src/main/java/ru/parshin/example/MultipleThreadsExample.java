package ru.parshin.example;

public class MultipleThreadsExample {
    public static void main(String[] args) {
        NewThread t1 = new NewThread();
        t1.setName("Поток-1");
        NewThread t2 = new NewThread();
        t2.setName("Поток-2");
        t1.start();
        t2.start();
    }
}
