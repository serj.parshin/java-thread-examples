package ru.parshin.example;

public class NewThread extends Thread {
    public void run() {
        long startTime = System.currentTimeMillis();
        int i = 0;
        while (true) {
            System.out.println(this.getName() + ": Новый поток запущен..." + i++);
            try {
                //Ждем одну секунду, чтобы не печаталось так часто
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //...
        }
    }
}
